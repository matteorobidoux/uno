public class NumberedCards extends ColoredCards {
    protected int value;

    public NumberedCards(String color, int value) {
        super(color);
        this.value = value;
    }

    @Override
    public Boolean canPlay(Uno card) {
        if(card instanceof ColoredCards) {
            if(card instanceof NumberedCards) {
                if (this.value == ((NumberedCards)card).getValue()) {
                    return true;
                }
            }
                if(this.color == ((ColoredCards)card).getColor()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

    public int getValue() {
        return this.value;
    }

    public void setValue(int newValue) {
        this.value = newValue;
    }
}
