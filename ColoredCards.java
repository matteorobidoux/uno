public abstract class ColoredCards implements Uno {
    protected String color;

    public ColoredCards(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
