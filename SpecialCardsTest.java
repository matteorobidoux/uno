import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SpecialCardsTest {

    @Test
    public void testGetSpecial() {
        SpecialCards card = new SkipCard("Blue", "PickUp2");
        assertEquals("PickUp2",card.getSpecial());
    }

    @Test
    public void testGetColor() {
        SpecialCards card = new SkipCard("Blue", "Skip");
        assertEquals("Blue",card.getColor());
    }

    @Test
    public void testCanPlayColor(){
        SpecialCards card = new ReverseCard("Blue", "Reverse");
        SpecialCards card2 = new SkipCard("Blue", "Skip");
        assertEquals(true,card.canPlay(card2));
    }

    @Test
    public void testCanPlaySpecial(){
        SpecialCards card = new SkipCard("Red", "Skip");
        SpecialCards card2 = new SkipCard("Blue", "Skip");
        assertEquals(true,card.canPlay(card2));
    }

    @Test
    public void testCanPlayNotEqual(){
        SpecialCards card = new ReverseCard("Red", "Reverse");
        SpecialCards card2 = new SkipCard("Blue", "Skip");
        assertEquals(false,card.canPlay(card2));
    }
}
