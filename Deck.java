import java.util.Random;
import java.util.ArrayList;

public class Deck {
    protected ArrayList<Uno> deck;
    protected WildCard wildCard;
    protected WildPickUp4Card wildPickUp4Card;
    protected NumberedCards numberedCards;
    protected SkipCard skipCard;
    protected PickUp2Card pickUp2Card;
    protected ReverseCard reverseCard;
    protected String[] colors;

    public Deck() {
        this.deck = new ArrayList<Uno>();
        this.wildCard = new WildCard();
        this.wildPickUp4Card = new WildPickUp4Card();
        this.colors = new String[]{"Yellow", "Red", "Blue", "Green"};

        for(int x = 0; x < 4; x++) {
            for(int j = 0; j < 10; j++) {
                numberedCards = new NumberedCards(colors[x], j);
                for(int i = 0; i < 2; i++) {
                    deck.add(numberedCards);
                }
            }
        }
        
        for(int j = 0; j < 4; j++) {
            deck.add(wildCard);
            deck.add(wildPickUp4Card);
            skipCard = new SkipCard(colors[j], "Skip");
            reverseCard = new ReverseCard(colors[j], "Reverse");
            pickUp2Card = new PickUp2Card(colors[j], "PickUp2");
            for(int i = 0; i < 2; i++) {
                deck.add(skipCard);
                deck.add(reverseCard);
                deck.add(pickUp2Card);
            }
        }
    }

    public void addToDeck(Uno card) {
        deck.add(0,card);
    }

    public Uno draw() {
        Uno card = deck.remove(deck.size()-1);
        return card;
    }

    public void shuffle() {
        for(int i=0; i< deck.size(); i++) {
            Random rand = new Random();
            int randomNumber = rand.nextInt(111-0);
            Uno temp = deck.get(i);
            deck.set(i,deck.get(randomNumber));
            deck.set(randomNumber,temp);
        }
    }
}