import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class NumberedCardsTest {
    @Test
    public void testGetValue() {
        NumberedCards card = new NumberedCards("Blue", 1);
        assertEquals(1,card.getValue());
    }

    @Test
    public void testGetColor() {
        NumberedCards card = new NumberedCards("Green", 1);
        assertEquals("Green",card.getColor());
    }

    @Test
    public void testCanPlayColor(){
        NumberedCards card = new NumberedCards("Blue", 1);
        NumberedCards card2 = new NumberedCards("Blue", 6);
        assertEquals(true,card.canPlay(card2));
    }

    @Test
    public void testCanPlayValue(){
        NumberedCards card = new NumberedCards("Blue", 3);
        NumberedCards card2 = new NumberedCards("Red", 3);
        assertEquals(true,card.canPlay(card2));
    }

    @Test
    public void testCanPlayNotEqual(){
        NumberedCards card = new NumberedCards("Blue", 3);
        NumberedCards card2 = new NumberedCards("Red", 9);
        assertEquals(false,card.canPlay(card2));
    }

    @Test
    public void testCanPlayInstanceOfColoredCards(){
        NumberedCards card = new NumberedCards("Blue", 3);
        PickUp2Card card2 = new PickUp2Card("Blue", "PickUp2");
        assertEquals(true,card.canPlay(card2));
    }
}
