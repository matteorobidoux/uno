import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class DeckTest {
    
    @Test
    public void testAddToDeck() {
        Deck deck1 = new Deck();
        NumberedCards card = new NumberedCards("Blue", 1);
        deck1.addToDeck(card);
        assertEquals(card,deck1.deck.get(0));
    }

    @Test
    public void testDrawFromDeck() {
        Deck deck1 = new Deck();
        assertEquals(deck1.deck.get(deck1.deck.size()-1),deck1.draw());
    }

    @Test
    public void testShuffleDeck() {
        Deck deck1 = new Deck();
        Uno card = deck1.deck.get(0);
        deck1.shuffle();
        assertNotEquals(card,deck1.deck.get(0));
    }
}
