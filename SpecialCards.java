public abstract class SpecialCards extends ColoredCards {
    protected String special;

    public SpecialCards(String color, String special) {
        super(color);
        this.special = special;
    }

    @Override
    public Boolean canPlay(Uno card) {
        if(card instanceof ColoredCards) {
            if(card instanceof SpecialCards) {
                if (this.special == ((SpecialCards)card).getSpecial()) {
                    return true;
                }
            }
            if(this.color == ((ColoredCards)card).getColor()) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public String getSpecial() {
        return this.special;
    }
}
