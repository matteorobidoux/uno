import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class WildCardTest {
    
    @Test
    public void testCanPlayParameterWild() {
        WildCard card = new WildCard();
        NumberedCards card2 = new NumberedCards("Blue", 1);
        assertEquals(false,card2.canPlay(card));
    }

    @Test
    public void testCanPlayCalledOnWild() {
        WildCard card = new WildCard();
        NumberedCards card2 = new NumberedCards("Blue", 1);
        assertEquals(true,card.canPlay(card2));
    }
}
